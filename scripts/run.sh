#! /usr/bin/env sh

#┌─────────────────────────────────────────────────────────────────────────────┐
#│ PasteIt - A paste client                                                    │
#│ ========================                                                    │
#│ File   : pasteit/scripts/run.sh                                             │
#│ License: Mozilla Public License 2.0                                         │
#│ URL    : https://gitlab.com/shivanandvp/pasteit                             │
#│ Authors:                                                                    │
#│     1. shivanandvp <shivanandvp@rebornos.org>                               │
#│     2.                                                                      │
#│ -----                                                                       │
#│ Description:                                                                │
#│                                                                             │
#│ A script to run the built binary using cargo run. All arguments passed to   │
#│ this script will be passed-through to `cargo run -- `.                      │
#│ -----                                                                       │
#│ Last Modified: Sat, 8th January 2022 4:19:39 PM -06:00                      │
#│ Modified By  :                                                              │
#│ -----                                                                       │
#│ 1. Copyright (c) 2021 shivanandvp <shivanandvp@rebornos.org>                │
#│ 2.                                                                          │
#└─────────────────────────────────────────────────────────────────────────────┘

SCRIPT_DIRECTORY="$(dirname -- "$(readlink -f -- "$0")")"
PROJECT_DIRECTORY="$(dirname -- "$SCRIPT_DIRECTORY")"

( # Create subshell to nullify directory changes on exit
    # Run makepkg
    set -o xtrace
    cd "$PROJECT_DIRECTORY" && \
    cargo run -- "$@"
    set +o xtrace
)